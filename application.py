import os
from flask_cors import CORS
import face_recognition
import numpy as np
from flask import Flask, request, jsonify
from werkzeug.utils import secure_filename

from face_recognition_api import recognize_face, load_known_faces
from werkzeug.exceptions import BadRequest
import datetime

application = app = Flask(__name__)
CORS(app)
app.config['UPLOAD_FOLDER'] = 'known_faces'  # Ensure this directory exists
app.config['ALLOWED_EXTENSIONS'] = {'png', 'jpg', 'jpeg'}

load_known_faces()

sessions_store = {}


@app.route('/check-in', methods=['POST'])
def check_in():
    try:
        employee_id = handle_face_recognition_event()
        if not employee_id:
            return jsonify({'message': 'Employee not recognized'}), 404

        if employee_id in sessions_store:
            return jsonify({'message': 'Employee has already checked in.'}), 400

        sessions_store[employee_id] = {
            'check_in': datetime.datetime.now(),
            'breaks': [],
            'check_out': None
        }
        return jsonify({
            'employee_id': employee_id,
            'message': 'Good morning!',
            'timestamp': str(datetime.datetime.now())
        }), 200
    except BadRequest as e:
        return jsonify({'message': str(e)}), 400

    except Exception as e:
        return jsonify({'message': 'An error occurred during check-in.'},e), 500


@app.route('/check-out', methods=['POST'])
def check_out():
    try:
        employee_id = handle_face_recognition_event()
        if not employee_id:
            return jsonify({'message': 'Employee not recognized'}), 404

        session = sessions_store.get(employee_id)
        if not session or session['check_out']:
            return jsonify({'message': 'Employee has not checked in or already checked out.'}), 400

        # Record checkout time
        session['check_out'] = datetime.datetime.now()
        work_duration, total_break_time = calculate_durations(session)

        # Clear the session after checking out or store the checkout status as needed
        del sessions_store[employee_id]  # This removes the session entirely

        # Alternatively, if you want to keep the record but mark as checked out, you could do:
        # sessions_store[employee_id]['checked_in'] = False

        return jsonify({
            'employee_id': employee_id,
            'message': 'Goodbye!',
            'total_hours_worked': work_duration,
            'total_break_time': total_break_time,
            'timestamp': str(datetime.datetime.now())
        }), 200
    except BadRequest as e:
        return jsonify({'message': str(e)}), 400
    except Exception as e:
        return jsonify({'message': 'An error occurred during check-out.'}, str(e)), 500



@app.route('/break', methods=['POST'])
def break_time():
    try:
        employee_id = request.form.get('employee_id')
        action = request.form.get('action')  # 'start' or 'end'

        if not employee_id or action not in ['start', 'end']:
            raise BadRequest("Missing or invalid 'employee_id' or 'action' in form data")

        if employee_id not in sessions_store:
            raise BadRequest("Employee session not found.")

        current_time = datetime.datetime.now()
        session = sessions_store[employee_id]

        if action == 'start':
            if session.get('current_break'):
                raise BadRequest("Break has already started.")
            session['current_break'] = current_time
        elif action == 'end':
            if not session.get('current_break'):
                raise BadRequest("No break has started.")
            session['breaks'].append((session['current_break'], current_time))
            del session['current_break']

        return jsonify({'employee_id': employee_id, 'action': action, 'timestamp': str(current_time)}), 200
    except BadRequest as e:
        return jsonify({'message': str(e)}), 400

    except Exception as e:
        return jsonify({'message': 'An error occurred during break time handling.'},e), 500


def handle_face_recognition_event():
    if 'image' not in request.files:
        raise BadRequest("Missing 'image' file in request")

    file = request.files['image']
    if file.filename == '':
        raise BadRequest("No selected file")

    try:
        return recognize_face(file)
    except Exception:
        raise BadRequest("Could not process the image for face recognition.")


def calculate_durations(session):
    work_duration = (session['check_out'] - session['check_in']).total_seconds() / 3600.0
    total_break_time = sum((end - start).total_seconds() for start, end in session['breaks']) / 3600.0
    return work_duration, total_break_time


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']


def add_new_face(employee_id, file_path):
    # This function should take the file path of the new face and process it.
    # It should encode the face and store the encoding for later recognition.
    # This is just a placeholder function to show where to put the processing logic.
    image = face_recognition.load_image_file(file_path)
    face_encodings = face_recognition.face_encodings(image)
    if face_encodings:
        # Save the encoding with associated employee ID
        # You'll need to decide on a format and storage method for these encodings
        # This could be as simple as a file per employee, a database, etc.
        encoding_file = os.path.join(app.config['UPLOAD_FOLDER'], f'{employee_id}.npy')
        np.save(encoding_file, face_encodings[0])
        return True
    return False


@app.route('/add-face', methods=['POST'])
def add_face():
    employee_id = request.form['employee_id']
    file = request.files['image']

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file_path = os.path.join(app.config['UPLOAD_FOLDER'], employee_id + '_' + filename)
        file.save(file_path)

        if add_new_face(employee_id, file_path):
            return jsonify({'message': 'New face added successfully.'}), 200
        else:
            return jsonify({'message': 'Failed to process the image.'}), 400
    else:
        return jsonify({'message': 'Invalid file type or missing image.'}), 400


if __name__ == '__main__':
    app.run(debug=True)
