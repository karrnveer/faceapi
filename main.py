import cv2
import face_recognition
import numpy as np
import os

# Load employee images and create face encodings
known_face_encodings = []
known_face_ids = []


def load_employee_faces():
    for filename in os.listdir('known_faces'):
        if filename.endswith('.jpg') or filename.endswith('.png'):
            image = face_recognition.load_image_file(f'known_faces/{filename}')
            face_encoding = face_recognition.face_encodings(image)[0]
            employee_id = os.path.splitext(filename)[0]
            known_face_encodings.append(face_encoding)
            known_face_ids.append(employee_id)


def check_in_employee():
    # Load the known faces
    load_employee_faces()

    # Start the webcam
    video_capture = cv2.VideoCapture(0)

    # Initialize variables
    face_locations = []
    face_encodings = []
    process_this_frame = True

    while True:
        # Grab a single frame of video
        ret, frame = video_capture.read()

        # Resize frame of video to 1/4 size for faster face recognition processing
        small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

        # Only process every other frame of video to save time
        if process_this_frame:
            # Find all the faces and face encodings in the current frame of video
            face_locations = face_recognition.face_locations(small_frame)
            face_encodings = face_recognition.face_encodings(small_frame, face_locations)

            # Check each face found in the frame
            for face_encoding in face_encodings:
                # See if the face is a match for the known faces
                matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
                name = "Unknown"

                # Use the known face with the smallest distance to the new face
                face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
                best_match_index = np.argmin(face_distances)
                if matches[best_match_index]:
                    name = known_face_ids[best_match_index]

                    # Stop from the loop, as we have recognized the employee
                    print(f"Employee {name} checked in/out.")
                    video_capture.release()
                    cv2.destroyAllWindows()
                    return name  # or do other stuff like logging the timestamp, etc.

        process_this_frame = not process_this_frame

        # Display the resulting image
        cv2.imshow('Video', frame)

        # Hit 'q' on the keyboard to quit!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Release the webcam
    video_capture.release()
    cv2.destroyAllWindows()
    return None


employee_id = check_in_employee()
if employee_id:
    print(f"Employee {employee_id} recognized.")
else:
    print("Employee not found.")
