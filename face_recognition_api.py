import face_recognition
import numpy as np
import os

# This directory should contain one image for each known person, with the file name being their employee ID.
KNOWN_FACES_DIR = 'known_faces'

known_face_encodings = []
known_face_ids = []


def load_known_faces():
    for filename in os.listdir(KNOWN_FACES_DIR):
        if filename.endswith('.jpg') or filename.endswith('.png'):
            image_path = os.path.join(KNOWN_FACES_DIR, filename)
            image = face_recognition.load_image_file(image_path)
            face_encoding = face_recognition.face_encodings(image)[0]

            known_face_encodings.append(face_encoding)
            known_face_ids.append(os.path.splitext(filename)[0])


def recognize_face(file_stream):
    # Load the uploaded image file
    img = face_recognition.load_image_file(file_stream)
    # Detect faces in the image
    face_encodings = face_recognition.face_encodings(img)

    if face_encodings:
        face_encoding = face_encodings[0]
        # See if the face is a match for the known face(s)
        matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
        face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
        best_match_index = np.argmin(face_distances)
        if matches[best_match_index]:
            return known_face_ids[best_match_index]

    return None
